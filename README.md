# vue-cli-web-component-implement-tutorial

## Project setup
```
npm install

vue create project-name
```

## After creating project
```
Delete HelloWorld.vue

Delete every code from App.vue which belongs to HelloWorld component

```

## Implementing web component
```
Copy dist folder from vue cli project in which you created web component

create a folder of web-components in src folder and paste dist there
```

## Using web component
```
import Helloworld from './src/web-components/dist/hello-world.js'
use this code inside App.vue script tag

and register as normal component

Lastly use custom tag in template
```

## Check web component
```
npm run serve
```
# Thank u